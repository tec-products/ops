#!/bin/bash

# Create Cluster
docker run --rm -it -v ${PWD}:/cluster -v ${HOME}/.ssh:/tmp/.ssh vitobotta/hetzner-k3s:v0.5.0 create-cluster --config-file /cluster/hs_prod.yaml
